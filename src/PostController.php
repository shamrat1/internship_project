<?php

namespace App;

use App\Database as DB;
use App\Message;
use App\Utility;

use PDO;
use PDOException;


class PostController extends DB
{

    public $id = "";

    public $title = "";

    public $body = "";

    public $user_id = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('title',$data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('body',$data)){
            $this->body = $data['body'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id = $data['user_id'];
        }
    }

    public function view(){
        $STH = $this->conn->query('SELECT * from `posts` WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }
    

    public function update(){

        $data = array($this->title,$this->body);
        $STH = $this->conn->prepare("UPDATE `posts` SET `title` = ? , `body` = ?  WHERE `id` =".$this->id);
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center' style='color: white'>[ Title: $this->title ] , [ Body: $this->body ] <br> Data Has Been Updated Successfully!</h3></div>");
        Utility::redirect('index.php');
    }


    public function store(){
        $DBH = $this->conn;
        
        $data = array($this->title,$this->body,$this->user_id);
        $STH = $DBH->prepare("INSERT INTO `posts`(`id`, `title`, `body`,`user_id`) VALUES (NULL ,?,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center' style='color: white'>[ Title: $this->title ] , [ Body: $this->body ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

        
    }

    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `posts` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center' style='color: white'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }
    public function index(){
        try{
            $STH = $this->conn->query('SELECT * from posts');


        $mode = $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;
        }
        catch (PDOException $e) {
            print $e->getMessage();
          }
        
    }
    public function trashed($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * from book_title WHERE `is_delete`= 1');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;
    }
    public function recover(){
        try{
            $query = "UPDATE `book_title` SET `is_delete` = ? WHERE `book_title`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(0,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center' style='color: white'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('trashed.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }
    }


    public function recoverSeleted($IDs = Array())
    {
        try {
            if ((is_array($IDs)) && (count($IDs > 0))) {
                $ids = implode(",", $IDs);
                $query = "UPDATE `atomic_project_b37`.`book_title` SET `is_delete` = 0 WHERE `id` IN(" . $ids . ")";
                $STH = $this->conn->prepare($query);
                 $STH->execute();

                if ($STH) {
                    Message::message("<div id='msg'></div><h3 align='center' style='color: white'> Data Has Been Recovered Successfully!</h3></div>");
                    Utility::redirect('index.php');
                }

            }
        }catch(PDOException $e){
            echo 'ERROR:'.$e->getMessage();
        }




}
    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_delete = 1 LIMIT $start,$itemsPerPage";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();
    public function deleteMultiple($IDs = Array())
    {
        try {
            if ((is_array($IDs)) && (count($IDs > 0))) {
                $ids = implode(",", $IDs);
                $query = "DELETE FROM `atomic_project_b37`.`book_title` WHERE `id`  IN(" . $ids . ")";
                $STH = $this->conn->prepare($query);
                 $STH->execute();

                if ($STH) {
                    Message::message("<div id='msg'></div><h3 align='center' style='color: white'> Data Has Been Recovered Successfully!</h3></div>");
                    Utility::redirect('index.php');
                }

            }
        }catch(PDOException $e){
            echo 'ERROR:'.$e->getMessage();
        }
    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_delete = 0 LIMIT $start,$itemsPerPage";

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();
}
