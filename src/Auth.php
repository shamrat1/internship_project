<?php
namespace App;

use App\Database as DB;
use App\Message;
use App\Utility;
use PDO;
use PDOException;
use PHPMailer;

class Auth extends DB{

    public $id = "";

    public $name = "";

    public $email = "";

    public $password = "";

    public $token = "";

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password = md5($data['password']);
        }
        if(array_key_exists('token',$data)){
            $this->token = $data['token'];
        }
        
    }

    public function register(){
        $DBH = $this->conn;
        $this->token = md5(uniqid(rand(), true));
        $data = array($this->name,$this->email,$this->password,$this->token);
        $STH = $DBH->prepare("INSERT INTO `users`(`id`, `name`,`email`, `password`,`token`) VALUES (NULL ,?,?,?,?)");
        $STH->execute($data);
       
        
        Message::message("<div id='msg'></div><h3 align='center' style='color: white'>Account has been created. Check your email for verification.</h3></div>");
        Utility::redirect('index.php');

    }

    public function login(){
        
        try{
        $DBH = $this->conn;
        $sql ="SELECT * FROM `users` WHERE `email`= :email AND `isVerified`=1";
        $STH = $DBH->prepare($sql);
        $STH->execute(array(
            'email' => $this->email
        ));
        $count = $STH->rowCount();
        if($count > 0){
            $row = $STH->fetch(PDO::FETCH_OBJ);

            if($this->password == $row->password){

            
                $_SESSION['name'] = $row->name;
                $_SESSION['id'] = $row->id;
                $_SESSION['email'] = $row->email;
                
                
            header('location:view/post/index.php');
            }
            else{
                Message::message("<div id='msg'></div><h3 align='center' style='color: white'>Password is incorrect.</h3></div>");
                Utility::redirect('index.php');
            }
            
        }
        else{
            Message::message("<div id='msg'></div><h3 align='center' style='color: RED'>Account is not verified.</h3></div>");
            Utility::redirect('index.php');
         }
        }
        catch (PDOException $e) {
            print $e->getMessage();
          }
        // $STH->setFetchMode(PDO::FETCH_OBJ);
        // $objAllData = $STH->fetchAll();
    }

    

    public function verify(){
        try{
            $DBH = $this->conn;
        $sql ="SELECT * FROM `users` WHERE `email`= :email AND `token`= :token";
        $STH = $DBH->prepare($sql);
        $STH->execute(array(
            'email' => $this->email,
            'token' => $this->token
        ));
        $count = $STH->rowCount();
        if($count > 0){
            $updateSql = "UPDATE `users` SET `isVerified`='1' WHERE `email`=:email AND `token`=:token";
            $STH = $DBH->prepare($updateSql);
            $STH->execute([
                'email' => $this->email,
                'token' => $this->token
            ]);
            Message::message("<div id='msg'></div><h3 align='center' style='color: white'>Account is verified, continue login.</h3></div>");
        Utility::redirect('index.php');
        }

        }
        catch (PDOException $e) {
            print $e->getMessage();
          }
    }
}
?>