<?php
require('vendor\autoload.php');
use App\Auth;
session_start();
if(isset($_POST['submit'])){
    if(empty($_POST["email"]) || empty($_POST["password"])){
        echo "No field can be left blank";
    }
    else{
        $objAuth = new Auth;
        $objAuth->setData($_POST);
        $objAuth->login();
    }
}
