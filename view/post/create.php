<?php
require_once("../../vendor/autoload.php");
use App\Message;
session_start();
if($_SESSION['email']){
if(!isset( $_SESSION)) session_start();
echo "<div class=\"message\">".Message::message()."</div>";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add New Post</title>
    <link rel="stylesheet" href="../../resource/assets/css/style.css">

    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/animate.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <link rel="stylesheet" href="../../resource/assets/css/media-queries.css">
    <style>


        body{
            background: url(../../resource/assets/css/img/img.jpg);
            background-size: 100%;
            background-repeat: no-repeat;
        }
        th{
            color: black;
        }
        table{
            padding-top:40px;
        }
        .form-control{
            border-radius: 5px;
            color: black;
        }
        .btn{
            border-radius: 5px;
            width: 100%;
            color: white;
        }
        h2{
            text-align: center;
            color: black;
        }
        form{
            background-color:transparent;
        }
    </style>

</head>
<body>
<div class="col-md-2">
<a href="index.php" class="btn btn-success">All Posts</a><a href="trashed.php" class="btn btn-info">Trash List</a>

    <a href="../../logout.php" class="btn btn-danger">Log Out</a></div>
<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <h2>Create A New Post</h2>

        <div class="col-md-8 col-md-offset-3">
<form class="form-horizontal" method="post" action="store.php">
    <div class="form-group">
        <label class="control-label col-sm-2" for="title" style="color: white">Title:</label>
        <div class="col-sm-6">
            <input type="text" name="title" class="form-control" id="title" placeholder="Enter Title" size="10px">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="body" style="color: white">Body:</label>
        <div class="col-sm-6">
        <textarea name="body" class="form-control" rows="5" from="title"></textarea>
        </div>
    </div>

    
            <input type="hidden" name="user_id" class="form-control" value="<?php echo $_SESSION['id']?>">
    

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-6">
            <button type="submit" name="submit" class="btn btn-info">Create</button>
        </div>
    </div>

</form>
        </div>
<script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../resource/assets/js/scripts.js"></script>

</body>
</html>

<script>
//    $('#message').show().delay(10).fadeOut();
//    $('#message').show().delay(10).fadeIn();
//    $('#message').show().delay(10).fadeOut();
//    $('#message').show().delay(10).fadeIn();
//    $('#message').show().delay(1200).fadeOut();
setTimeout(fade_out, 4000);

function fade_out() {
    $("#message").fadeOut().empty();
}
</script>

<?php }
else{
    header("location:../../index.php");
}
?>


