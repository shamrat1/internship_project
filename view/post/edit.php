<?php
//var_dump($_GET);
include_once('../../vendor/autoload.php');
use App\PostController;
use App\Utility;
session_start();
if($_SESSION['email']){
$obj= new PostController();
$obj->setData($_GET);
$oneData = $obj->view();
foreach($oneData as $singleItem) {
    $id = $singleItem->id;
    $title = $singleItem->title;
    $body = $singleItem->body;
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Post</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/animate.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <link rel="stylesheet" href="../../resource/assets/css/media-queries.css">
    <style>

        body{
            background: url(../../resource/assets/css/img/img.jpg);
            background-size: 100%;
            background-repeat: no-repeat;
        }
        th{
            color: black;
        }
        table{
            padding-top:40px;
        }
        .form-control{
            border-radius: 5px;
            color: black;
        }
        .btn{
            border-radius: 5px;
            width: 100%;
            color: white;
        }
        h2{
            text-align: center;
            color: black;
        }
        form{
            background-color:transparent;
        }
        .container{
            padding-top: 40px;
        }
        .btn{
            width: 30%;
            float: left;
        }

    </style>
</head>
<body>
<a href="index.php" class="btn btn-success">All Posts</a><a href="trashed.php" class="btn btn-info">Trash List</a>

    <a href="../../logout.php" class="btn btn-danger">Log Out</a>
<div class="container">

    <h2>Edit Post</h2>

    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
            <input type="hidden" name="id" id="id" value="<?php echo $id?>">
            <input type="text" name="title" class="form-control" id="title"  value="<?php echo $title?>">
            <input type="text" name="body" class="form-control" id="body" value="<?php echo $body?>">
        <?php } ?>


        <button type="submit" value="submit" class="btn btn-info">Update</button>
        </div>
        </div>
    </form>
</div>

</body>
</html>
<?php }
else{
    header("location:../../index.php");
}
?>