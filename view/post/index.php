<?php
include_once("../../vendor/autoload.php");
session_start();
use App\PostController;
use App\Message;
if($_SESSION['email']){

$obj = new PostController();
$allData = $obj->index();
$serial = 0;
if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">".Message::message()."</div>";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <script>function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }</script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/animate.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <link rel="stylesheet" href="../../resource/assets/css/media-queries.css">
    <style>

        body{
            background: url(../../resource/assets/css/img/img.jpg);
            background-size: 100%;
            background-repeat: no-repeat;
        }
        .row{
            color: black;
            padding-top:100px;
        }
        th{
            color: black;
        }
        table{
            padding-top:40px;
            background-color: #999999;
        }


    </style>

</head>
<body>
<div class="col-md-2">
<a href="index.php" class="btn btn-success">All Posts</a><a href="trashed.php" class="btn btn-info">Trash List</a>

    <a href="../../logout.php" class="btn btn-danger">Log Out</a></div>
<div class="container">
    <a href="create.php" class="btn btn-default">Add New</a>
    
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form action="trashmultiple.php" method="post" id="multiple">
                <button type="submit" class="btn btn-info" >Trash Selected</button>
                <button type="submit" class="btn btn-primary" id="delete">Delete all Selected</button>
                <br><br>
        <table class="table table-striped">
    <thead>
    <tr>
        <th><input id="checkAll" type="button" value="Check All"></th>
        <th>SERIAL</th>
        <th>ID</th>
        <th>Post TITLE</th>
        <th>Body</th>
        <th class="col-sm-4">Actions</th>

    </tr>
    </thead>
    <?php foreach($allData as $oneData) {
    ?>
    <div class="col-sm-4">
    <tbody>
    <tr>
        <td><input type="checkbox" name="mark[]" value="<?php echo $oneData->id ?>"></td>
        <td><?php echo $serial++; ?></td>
        <td><?php echo $oneData->id ; ?></td>
        <td><?php echo $oneData->title ; ?></td>
        <td><?php echo $oneData->body ; ?></td>
        <td><a href="view.php?id=<?php echo $oneData->id ?>" class="btn btn-primary" role="button">View</a>
        <?php if($oneData->user_id == $_SESSION['id']){ ?>
            <a href="edit.php?id=<?php echo $oneData->id ?>" class="btn btn-info" role="button">Edit</a>
            <a href="delete.php?id=<?php echo $oneData->id ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
        <?php } ?>
        </td>    </tr>
    </tbody>
        </div>
<?php } ?>
    <script src="../../resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../resource/assets/js/jquery.backstretch.min.js"></script>
    <script src="../../resource/assets/js/scripts.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            </table>

        </div>
        </div>
    
    </body>
    </html>

    <script>
            $('#message').show().delay(10).fadeOut();

            $('#message').show().delay(10).fadeIn();
            $('#message').show().delay(1200).fadeOut();

    $('#delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
            $(function() {

                $(document).on('click', '#checkAll', function() {

                    if ($(this).val() == 'Check All') {
                        $('.button input').prop('checked', true);
                        $(this).val('Uncheck All');
                        $(':checkbox').each(function() {
                            this.checked = true;
                        });
                    } else {
                        $('.button input').prop('checked', false);
                        $(this).val('Check All');
                        $(':checkbox').each(function() {
                            this.checked = false;
                        });
                    }
                });

            });
</script>
<?php }
else{
    header("location:../../index.php");
}
?>